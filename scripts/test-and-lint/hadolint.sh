#!/usr/bin/env sh

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

HADOLINT="hadolint/hadolint:v2.9.3-alpine"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running Hadolint"
docker run --rm --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind -w "${DOCKER_PROJECT_DIR}" "${HADOLINT}" hadolint build/package/Dockerfile
