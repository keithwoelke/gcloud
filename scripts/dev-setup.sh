#!/usr/bin/env sh

set -e

[ -x "$(command -v git)" ] || { echo "Git is not installed." ; exit 1; }

PROJECT_DIR="$(cd "${0%/*}/.." && pwd -P)"

git config core.hooksPath "${PROJECT_DIR}/githooks"
